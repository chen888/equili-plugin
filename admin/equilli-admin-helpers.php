<?php


function get_session_token() {
    global $wpdb;

    $result = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}equilli_settings WHERE `option_name`='login_token'");
    return $result[0]->option_value;
}

function equilli_get_categories() {

    $ch = curl_init(API_URL . 'categories/menu');
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json'
    ));

    $result = curl_exec($ch);

    return json_decode($result);
}


function equilli_ids() {
    global $wpdb;

    $result = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}equilli_products");
    $ids_array = [];

    foreach($result as $r){
        $ids_array[] = $r->equilli_id;
    }

    $data_string = json_encode(['ids'=>$ids_array]);

    // return $ids_array;
    
    $ch = curl_init(API_URL . 'products/check-status');
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json'
    ));

    $result = curl_exec($ch);

    $decoded =  json_decode($result);
    return $decoded;
    

}


function equilli_search_product($string) {

    $data = array('query' => $string);
    $data_string = json_encode($data);

    $ch = curl_init(API_URL . 'search/products-mini');
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json'
    ));

    $result = curl_exec($ch);

    return json_decode($result);
}