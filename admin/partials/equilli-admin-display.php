<?php
include('../equilli-admin-helpers.php');
$categories = equilli_get_categories();
$status_array = equilli_ids();
?>
<script>
    var API_URL = "<?=API_URL?>";
</script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<style>
    .lds-roller{display:inline-block;position:relative;width:64px;height:64px}.lds-roller div{animation:lds-roller 1.2s cubic-bezier(0.5,0,0.5,1) infinite;transform-origin:32px 32px}.lds-roller div:after{content:" ";display:block;position:absolute;width:6px;height:6px;border-radius:50%;background:#0000d6;margin:-3px 0 0 -3px}.lds-roller div:nth-child(1){animation-delay:-.036s}.lds-roller div:nth-child(1):after{top:50px;left:50px}.lds-roller div:nth-child(2){animation-delay:-.072s}.lds-roller div:nth-child(2):after{top:54px;left:45px}.lds-roller div:nth-child(3){animation-delay:-.108s}.lds-roller div:nth-child(3):after{top:57px;left:39px}.lds-roller div:nth-child(4){animation-delay:-.144s}.lds-roller div:nth-child(4):after{top:58px;left:32px}.lds-roller div:nth-child(5){animation-delay:-.18s}.lds-roller div:nth-child(5):after{top:57px;left:25px}.lds-roller div:nth-child(6){animation-delay:-.216s}.lds-roller div:nth-child(6):after{top:54px;left:19px}.lds-roller div:nth-child(7){animation-delay:-.252s}.lds-roller div:nth-child(7):after{top:50px;left:14px}.lds-roller div:nth-child(8){animation-delay:-.288s}.lds-roller div:nth-child(8):after{top:45px;left:10px}@keyframes lds-roller{0%{transform:rotate(0deg)}100%{transform:rotate(360deg)}}#ajax_loader{position:fixed;left:50%;top:50%;transform:translate(-50%,-50%);transform:scale(2)}
    .equilli-product-row-title-container{
        padding-top: 10px;
    }
    #categories-container{
        margin-top: 10px;
    }
    #ajax_loader {
        position: fixed;
        left: 50%;
        top: 50%;
        transform: translate(-50%, -50%);
        transform: scale(2);
        z-index: 5;
    }
    #backdrop{
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100vh;
        background-color: rgba(0,0,0,0.5);
        z-index: 4;
        display: none;
    }
    h3.equilli-product-row-title{
        margin-bottom: 5px;
    }
    .equilli-resync-store-button{
        float: right;
        margin-top: -15px;
    }
    .equilli-product-row-categories-container-closed{
        margin-top: 0px;
    }
    .my_dropdown_categories{
        display:none;
        z-index: 5;
        position:absolute;
        background-color:#fff;
        padding:10px;
        border-radius:3px;
        border-left: 1px solid lightgray;
        border-right: 1px solid lightgray;
        border-bottom: 1px solid lightgray;
    }
    .toggle_option_group{
        font-size: 1.1rem;
        font-weight: 600;
    }
    .option_group{
        font-size: 1.1rem;
    }
    .option_group p, .toggle_option_group{
        margin: 0;
        cursor: pointer;
        padding: 10px 10px 10px 20px;
    }
    .option_group p:hover, .toggle_option_group:hover{
        background-color: #E6E6E6;
    }
    .logout{
        border: 1px solid #f26524;
        color: #000;
        border-radius: 5px;
        padding: 10px 10px;
        float: right;
    }
    .equilli-product-row-edit{
        display: block;
        margin: -25px 20px 20px 20px;
    }
    .select-category{
        width: 150px;
    }
    .equilli-product-row-action-text{
        margin-top: 4px;
    }
    .second-equilli-save-products-button{
        float: right;
        margin-right: 15px;
        margin-top: -15px;
    }
</style>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
<div id="backdrop">
    <div id="ajax_loader" class="lds-roller">
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
    </div>
</div>
<h1>Equilli Affiliate</h1>

<div class="equilli-main-container">

    <form id="sync-form">
        <h2 class="equilli-inner-title">Choose a button style</h2>
        <button class="logout" id="logout" type="button">Logout</button>
        <p>The button will appear below your call to action button (e.g. "add to cart"). with an explanatory invite to "name your price".</p>

        <div>
            <button class="equilli-select-button-1 equilli-select-button" type="button" button-type="1">&nbsp;</button>
            <button class="equilli-select-button-2 equilli-select-button" type="button" button-type="2">&nbsp;</button>
            <button class="equilli-select-button-3 equilli-select-button" type="button" button-type="3">&nbsp;</button>
        </div>

        <h2 class="equilli-inner-title">Choose products to enable equili plugin</h2>
        <p>your products were compered with exicting equili products, the ones that were matched are automaticly filled in (dont worry you can still change & edit them), in the table below you need to fill in additional details and edit existing ones to your liking. you can also set different types of alerts & auto actions for each product.</p>
        <button type="button" class="equilli-resync-store-button">Re-scan my store</button>
        <button class="equilli-save-products-button second-equilli-save-products-button">Save</button>

        <div class="equilli-select-all-container">
            <input type="checkbox" id="equilli-select-all">
            <p>Select All</p>
        </div>
        <?php 
			$start = microtime();
			foreach($products_array as $key => $item): 
			?>

            <?php
            if(!$_GET['resync'] && !isset($local_products[$item->get_id()])) {
                continue;
            }

            $prod_id; $prod_status = ''; $color = '';

            foreach($status_array as $st){
                if($local_products[$item->get_id()]->equilli_id == $st->id){
                    $prod_status = $st->status;
                    $prod_id     = $st->id;
                }
            }

            switch($prod_status){
                case 'Approved':
                    $color = '#00b208';
                break;
                case 'Pending':
                    $color = '#667cff';
                break;
                case 'Not Approved':
                    $color = '#ff4c4c';
                break;  
            }
            
            ?>

            <div class="equilli-product-row-container<?=(($key % 2 === 0) ? ' dark' : '')?>">
                <p class="product_status" style="float:right;font-size:24px;font-weight:800;margin:5px;color:<?=$color?>"><?= ucfirst($prod_status) ?> </p>
                <?php if($prod_status != '' && $prod_status != 'Approved'): ?>
                    <br style="clear:both">
                    <button type="button" class="btn btn-primary btn-sm pull-right approve_product" data-id="<?=$prod_id?>" style="margin-right:7px">Approve</button>
                <?php endif; ?>
                <div class="equilli-product-row-inner-container">
                    <div class="equilli-product-row-checkbox-container">
                        <input type="checkbox" class="equilli-product-checkbox" value="1" name="is-active[<?=$item->get_id()?>]" <?= isset($local_products[$item->get_id()]) ? 'checked' : '' ?>>
                    </div>
                    <div class="equilli-product-row-title-container">
                        <input type="hidden" name="id[<?=$item->get_id()?>]" value="<?=$item->get_id()?>" />
                        <input type="hidden" name="name[<?=$item->get_id()?>]" value="<?=$item->get_title()?>" />
                        <input type="hidden" name="estimated_market_price[<?=$item->get_id()?>]" value="<?=$item->get_regular_price()?>" />
                        <input type="hidden" name="estimated_market_price_code[<?=$item->get_id()?>]" value="<?=get_woocommerce_currency()?>" />

                        <?php
                            $imageUrl = $item->get_image_id();
                            if($imageUrl) {
                                $imageUrl = wp_get_attachment_image_src($imageUrl)[0];
                                ?>
                                <input type="hidden" name="image[<?=$item->get_id()?>]" value="<?=$imageUrl?>" />
                                <?php
                            }
                        ?>

                        <h3 class="equilli-product-row-title">Product</h3>

                        <select class="equilli-product-row-match" name="product-match[<?=$item->get_id()?>]">
                            <option value="">Create new</option>
                            <?php

                            $similar = equilli_search_product($item->get_title());

                            foreach($similar as $index => $similar_item) {
                                ?>
                                <option value="<?=$similar_item->id?>" <?=$local_products[$item->get_id()]->equilli_id == $similar_item->id ? 'selected' : '' ?>><?=$similar_item->title?></option>
                                <?php
                            }
                            ?>
                        </select>
                        <div id="categories-container">
                            <h3 class="equilli-product-row-title">Category</h3>
                            <div class="equilli-product-row-categories-container-closed">
                                <!-- <span>
                                    <?php
                                    // foreach($categories as $parent_category) {

                                    //     foreach($parent_category->children as $child_category) {

                                    //         if($local_products[$item->get_id()]->category == $child_category->id) {
                                    //             echo $child_category->name;
                                    //             break;
                                    //         }
                                    //     }
                                    // }
                                    ?>
                                </span> -->
                                <select name="category[<?=$item->get_id()?>]" class="select-category">
                                    <option value="0">Choose Category</option>
                                    <?php

                                    $categories = equilli_get_categories();
                                    foreach($categories as $parent_category) {

                                    ?> 
                                    <optgroup label = "<?=$parent_category->name?>" > 
                                        <?php foreach($parent_category->children as $child_category) { ?>
                                            <option <?=($local_products[$item->get_id()]->category == $child_category->id) ? 'selected' : ''?> value="<?= $child_category->id ?>"><?= $child_category->name ?></option>
                                        <?php } ?>
                                    </optgroup>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <img class="equilli-product-row-image" src="<?=$imageUrl?>" />
                    <div class="equilli-product-row-right-side-container">
                        <div class="equilli-product-row-open-close">
                            <!-- <a href="javascript: void(0);" class="equilli-row-toggle" product-id="</?=//$item->get_id()?/>">Edit product's details</a> -->
                        </div>
                        <p style="font-weight:600;font-size:1.1rem; line-height:3rem;margin:0">
                        <?= $item->get_name() ?>&nbsp;&nbsp; <?= $item->get_price() ?> <?php echo $item->get_price() !== '' ? get_woocommerce_currency():''; ?>
                        </p>
                        <div class="equilli-product-row-actions-container">
                            <div class="equilli-product-row-action">
                                <input type="checkbox" name="take_bid_active[<?=$item->get_id()?>]" value="1" <?= $local_products[$item->get_id()]->take_bid ? 'checked' : ''?> />
                                <label class="equilli-product-row-action-label">take bid</label>
                                <span class="equilli-product-row-action-text-title extra-margin">min. price</span>
                                <input type="hidden" name="take_bid_minimum_price_code[<?=$item->get_id()?>]" value="<?=get_woocommerce_currency()?>" />
                                <input type="text" style="width:50px"  class="equilli-product-row-action-text" name="take_bid_minimum_price[<?=$item->get_id()?>]" value="<?= $local_products[$item->get_id()]->take_bid?>" />
                            </div>
                            <div class="equilli-product-row-action">
                                <input type="checkbox" name="alert_active[<?=$item->get_id()?>]" <?= $local_products[$item->get_id()]->alert ? 'checked' : ''?> value="1" />
                                <label class="equilli-product-row-action-label">alert</label>
                                <span class="equilli-product-row-action-text-title extra-margin">max. price</span>
                                <input type="hidden" name="alert_minimum_price_code[<?=$item->get_id()?>]" value="<?=get_woocommerce_currency()?>" />
                                <input type="text" style="width:50px" class="equilli-product-row-action-text" name="alert_minimum_price[<?=$item->get_id()?>]" value="<?= $local_products[$item->get_id()]->alert?>" />
                            </div>
                            <div class="equilli-product-row-action">
                                <input type="checkbox" name="counter_offer_active[<?=$item->get_id()?>]" value="1" <?= $local_products[$item->get_id()]->offer_min ? 'checked' : ''?> />
                                <label class="equilli-product-row-action-label">counter offer</label>
                                <div class="equilli-product-row-ranges-container">
                                    <span class="equilli-product-row-action-text-title" style="display: inline-block; margin-right: 50px;margin-top: 8px;">range</span>
                                    <span class="equilli-product-row-action-text-title" style="display: inline-block;margin-top: 8px;">counter price</span>
                                    <div></div>
                                    <input type="hidden" name="counter_offer_minimum_price_code[<?=$item->get_id()?>]" value="<?=get_woocommerce_currency()?>" />
                                    <input type="text" style="width: 50px; display: inline;margin-top: 0px;" name="counter_offer_minimum_price[<?=$item->get_id()?>]" class="equilli-product-row-action-text" value="<?= $local_products[$item->get_id()]->offer_min?>" />
                                    <span><</span>
                                    <input type="hidden" name="counter_offer_maximum_price_code[<?=$item->get_id()?>]" value="<?=get_woocommerce_currency()?>" />
                                    <input type="text" style="width: 50px; display: inline;margin-top: 0px;" name="counter_offer_maximum_price[<?=$item->get_id()?>]" class="equilli-product-row-action-text" value="<?= $local_products[$item->get_id()]->offer_max?>" />
                                    <input type="hidden" name="counter_offer_price_code[<?=$item->get_id()?>]" value="<?=get_woocommerce_currency()?>" />
                                    <input type="text" style="width: 50px; display: inline;margin-top: 0px;" name="counter_offer_price[<?=$item->get_id()?>]" class="equilli-product-row-action-text" value="<?= $local_products[$item->get_id()]->offer_price?>" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="equilli-product-row-edit" product-id="<?=$item->get_id()?>">
                    <!-- <h3 class="equilli-product-row-title">Category</h3> -->

                    <h3 class="equilli-product-row-title" style="margin-top: 10px">Product Description</h3>

                    <textarea class="equilli-product-description" name="description[<?=$item->get_id()?>]">
                        <?=strip_tags($item->get_description())?>
                    </textarea>
                </div>
            </div>
        <?php endforeach; 
			$end = microtime();
			die('start ' . $start . ' ' . 'end' . ' ' . $end . ' ' . number_format($end - $start, 5));
		?>
				

        <div class="equilli-save-products-button-container">
            <button type="submit" class="equilli-save-products-button">Save</button>
        </div>
    </form>

</div>
