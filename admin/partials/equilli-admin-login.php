<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
<div class="wrap">
    <h1>Equilli login</h1>
    <form id="login-form">
        <table class="form-table">
            <tbody>
            <tr>
                <th class="row">
                    <label for="email">Email</label>
                </th>
                <td>
                    <input type="text" name="email" id="email" class="regular-text" />
                </td>
            </tr>
            <tr>
                <th class="row">
                    <label for="password">Password</label>
                </th>
                <td style="position:relative">
                    <input type="password" name="password" id="password" class="regular-text" />
                    <i id="show_password" style="margin-left:-30px;padding: 5px" class="fa fa-eye" aria-hidden="true"></i>
                </td>
            </tr>
            <tr>
                <th class="row"></th>
                <td>
                    <input type="submit" class="button button-primary" value="Login"/><br /><br />
                    <a href="admin.php?page=equilli-register">Register</a>
                </td>
            </tr>
            </tbody>
        </table>
    </form>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
<script></script>



