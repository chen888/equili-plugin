<?php

include('equilli-admin-helpers.php');

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       Equilli
 * @since      1.0.0
 *
 * @package    Equilli
 * @subpackage Equilli/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Equilli
 * @subpackage Equilli/admin
 * @author     Equilli <Equilli>
 */
class Equilli_Admin {

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $plugin_name    The ID of this plugin.
     */
    private $plugin_name;

    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $version    The current version of this plugin.
     */
    private $version;

    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     * @param      string    $plugin_name       The name of this plugin.
     * @param      string    $version    The version of this plugin.
     */
    public function __construct( $plugin_name, $version ) {

        $this->plugin_name = $plugin_name;
        $this->version = $version;

    }

    /**
     * Register the stylesheets for the admin area.
     *
     * @since    1.0.0
     */
    public function enqueue_styles() {

        wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/equilli-admin.css', array(), $this->version, 'all' );
        wp_enqueue_style( $this->plugin_name . '-checkbox', plugin_dir_url( __FILE__ ) . 'css/checkboxes.css', array(), $this->version, 'all' );

    }

    /**
     * Register the JavaScript for the admin area.
     *
     * @since    1.0.0
     */
    public function enqueue_scripts() {

        wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/equilli-admin.js', array( 'jquery' ), $this->version, false );
        wp_enqueue_script( $this->plugin_name . '-checkbox', plugin_dir_url( __FILE__ ) . 'js/checkboxes.js', array( 'jquery' ), $this->version, false );

        wp_localize_script( 'equilli', 'equilli', array(
            'ajax_url' => admin_url( 'admin-ajax.php' )
        ));
    }

    /**
     * Add the equilli to admin menu.
     *
     * @since    1.0.0
     */
    public function add_menu() {
        add_menu_page( 'Equilli', 'Equilli', 'manage_options', 'equilli', 'Equilli_Admin::admin_page' );
        add_submenu_page(null, 'Equilli Register', 'Equilli Register', 'manage_options', 'equilli-register', 'Equilli_Admin::register');
    }

    static function validate_user() {
        global $wpdb;

        $result = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}equilli_settings WHERE `option_name`='login_token'");

        if($result[0]->option_value !== '') {
            if(Equilli_Admin::get_user($result[0]->option_value)) {
                return true;
            }

        }

        return false;
    }

    static function login() {
        global $wpdb;

        $data = array('email' => $_POST['payload']['email'], 'password' => $_POST['payload']['password'], 'remember' => false);
        $data_string = json_encode($data);

        $ch = curl_init(API_URL . 'auth/login');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
        );

        
        $result = curl_exec($ch);
        $result = json_decode($result);

        // echo json_encode($result); die();

        if(isset($result->access_token)) {
            $wpdb->query($wpdb->prepare("UPDATE {$wpdb->prefix}equilli_settings SET
                                        `option_value` = %s
                                        WHERE `option_name`='login_token'", [ $result->access_token ]));
            echo json_encode(['success' => true]);
            wp_die();
        } else {
            echo json_encode(['success' => false]);
            wp_die();
        }
    }

    static function logout(){
        global $wpdb;
        $wpdb->query($wpdb->prepare("UPDATE {$wpdb->prefix}equilli_settings SET `option_value` = %s WHERE `option_name`='login_token'", [ '' ]));
        echo json_encode(['success' => true]);
        wp_die();
    }

    static function sync() {
        global $wpdb;

        $token = get_session_token();

        $data = $_POST['payload'];
        
        $existing = [];

        foreach($data as $index => $item) {
            if($item['is-active'] != 1) {
                unset($data[$index]);
            }

            if($item['product-match'] != null && $item['product-match'] != '') {
                $existing[] = $data[$index];
                unset($data[$index]);
            }
        }
        /*
         * Change sync logic, on api only update if exist, and send equilli id if exists
         *
         * */
        
        if(count($data) > 0) {
            $data_string = json_encode(['data' => $data]);

            $ch = curl_init(API_URL . 'supplier/products/create/bulk');
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Authorization: Bearer ' . $token,
                'Content-Type: application/json'
            ));

            $result = curl_exec($ch);


            $parsedResult = json_decode($result);
        }

        $unsynchedProducts = [];
        $synchedProducts = [];
        $synced = [];
        $unsynced = [];

        $wpdb->query("DELETE FROM {$wpdb->prefix}equilli_products");

        if(count($data) > 0 && count($parsedResult)) {

            foreach ($parsedResult->products as $localProduct => $remoteProduct) {
                $_data = explode(',',$remoteProduct);
                $remote_product = $_data[0];
                $prod_status = $_data[1];
        
                 $product_data = json_encode(array(
                    'category'    => $data[$localProduct]['category'],
                    'description' => $data[$localProduct]['description'],
                    'alert'       => isset($data[$localProduct]['alert_minimum_price']) ? $data[$localProduct]['alert_minimum_price'] : null,
                    'take_bid'    => isset($data[$localProduct]['take_bid_minimum_price']) ? $data[$localProduct]['take_bid_minimum_price'] : null,
                    'offer_min'   => isset($data[$localProduct]['counter_offer_minimum_price']) ? $data[$localProduct]['counter_offer_minimum_price'] : null,
                    'offer_max'   => isset($data[$localProduct]['counter_offer_maximum_price']) ? $data[$localProduct]['counter_offer_maximum_price'] : null,
                    'offer_price' => isset($data[$localProduct]['counter_offer_price']) ? $data[$localProduct]['counter_offer_price'] : null,
                    'status'      => isset($prod_status) ? $prod_status : null  
                ));

                $prod_front = json_encode([
                    'id' => $data[$localProduct]['id'],
                    'name' => $data[$localProduct]['name'],
                    'price' => isset($data[$localProduct]['take_bid_minimum_price']) ? $data[$localProduct]['take_bid_minimum_price'] : null,
                ]);

                // echo $wpdb->prepare("INSERT INTO {$wpdb->prefix}equilli_products (`local_id`, `equilli_id`, `data`) 
                // VALUES (%d, %d, %s)", [
                //         $localProduct, $remoteProduct, $product_data
                //     ]);
                if ($wpdb->query($wpdb->prepare("INSERT INTO {$wpdb->prefix}equilli_products (`local_id`, `equilli_id`, `data`) 
                VALUES (%d, %d, %s)", [
                        $localProduct, $remote_product, $product_data
                    ])) === 0
                ) 
                {
                    $unsynchedProducts[] = $localProduct;
                    $unsynced[] = $prod_front;

                } else {
                    $synchedProducts[] = $localProduct;
                    $synced[] = $prod_front;
                }
            }
        }


        if(count($existing) > 0) {
            foreach ($existing as $item) {

                $product_data = json_encode(array(
                    'category' => $item['category'],
                    'description' => $item['description'],
                    'alert' => isset($item['alert_minimum_price']) ? $item['alert_minimum_price'] : null,
                    'take_bid' => isset($item['take_bid_minimum_price']) ? $item['take_bid_minimum_price'] : null,
                    'offer_min' => isset($item['counter_offer_minimum_price']) ? $item['counter_offer_minimum_price'] : null,
                    'offer_max' => isset($item['counter_offer_maximum_price']) ? $item['counter_offer_maximum_price'] : null,
                    'offer_price' => isset($item['counter_offer_price']) ? $item['counter_offer_price'] : null,
                ));
                
                $prod_front = json_encode([
                    'id' => $data[$localProduct]['id'],
                    'name' => $data[$localProduct]['name'],
                    'price' => isset($item['take_bid_minimum_price']) ? $item['take_bid_minimum_price'] : null,
                ]);

                if ($wpdb->query($wpdb->prepare("INSERT IGNORE INTO {$wpdb->prefix}equilli_products (`local_id`, `equilli_id`, `data`) 
                VALUES (%d, %d, %s)", [
                        $item['id'], $item['product-match'], $product_data
                    ])) === 0
                ) {
                    $unsynchedProducts[] = $item['id'];
                    $unsynced[] = $prod_front;
                } else {
                    $synchedProducts[] = $item['id'];
                    $synced[] = $prod_front;
                }
            }
        }

        echo json_encode([
            'existing' => $existing,
            'synced' => $synced,
            'unsynced' => $unsynced,
            'qwe' => $parsedResult
        ]);
    }

    static function save_button_type() {

        global $wpdb;

        $button = $_POST['payload']['button'];
        if($wpdb->query($wpdb->prepare("UPDATE `{$wpdb->prefix}equilli_settings` SET `option_value`=%s WHERE `option_name`='button_type'", [
                $button
            ])) === 0) {

            return true;
        } else {
            return false;
        }
    }

    static function get_user($token) {

        $authorization = 'Bearer ' . $token;

        $ch = curl_init(API_URL . 'auth/getUser');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Authorization: ' . $authorization
        ));

        curl_exec($ch);

        $result = curl_exec($ch);
        $result = json_decode($result);

        // print_r( $result['success'] );die();

        return isset($result->success) ? false : true ;
    }

    public static function register() {

        include 'partials/equilli-admin-register.php';
    }

    public static function admin_page() {

        global $wpdb;
        /*
         * User is not logged in to equilli
         * */
        if(!Equilli_Admin::validate_user()) {
            include 'partials/equilli-admin-login.php';

        } else {

            $products_array = wc_get_products([
                'orderby' => 'name',
                'status'  => 'publish',
                'posts_per_page' => '1000',
                'paged'          => '1'
            ]);

            $results = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}equilli_products");
            $local_products = [];

            foreach($results as $item) {
                $_data = json_decode($item->data);
                $_data->equilli_id = $item->equilli_id;
                $local_products[$item->local_id] = $_data;
            }

            include 'partials/equilli-admin-display.php';
        }
    }
}
