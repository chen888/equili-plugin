(function( $ ) {
    'use strict';

    
    function cal_swall(html, type) {
        const wrapper = document.createElement('div');
        wrapper.innerHTML = html;
        swal({
          title: type,
          content: wrapper, 
          icon: type.toLowerCase()
        });
    }

    $('.second-equilli-save-products-button').click(function(){
        $('#sync-form').submit();
    });

    $(document).on('click', '#equilli-select-all', function() {

        if($('#equilli-select-all').is(':checked')) {

            $('.equilli-product-checkbox').each(function(index, item) {

                $(item).parent().addClass('customCheckboxChecked');
                $(item).prop('checked', true);
            })

        } else {

            $('.equilli-product-checkbox').each(function(index, item) {

                $(item).parent().removeClass('customCheckboxChecked');
                $(item).prop('checked', false);
            })
        }

    });

    $(document).on('click', '.equilli-resync-store-button', function() {
        $('#backdrop').css('display', 'block');
        window.location.replace(window.location.href + '&resync=true');
    });

    window.addEventListener('message', function(e){
        console.log(e.data);
        
        switch(e.data.message){
            case 'success':
                window.location = "/wp-admin/admin.php?page=equilli";
            break;
            case 'successful-bid':
                window.history.back();
            break;
            case 'redirect-bid':
                window.location =  'https://equilli.devsense.co.il:8000/bid/'+e.data.bid_id
            break;
        }
    },false);

    // document.querySelector('body').addEventListener('click', function(e){alert(e)})

    $(document).on('submit', '#login-form', function(e) {
        e.preventDefault();

        var data = {
            'action': 'login',
            'payload': {
                email: $('#email').val(),
                password: $('#password').val()
            }
        };

        jQuery.post(equilli.ajax_url, data, function(response) {

            response = JSON.parse(response);
            console.log(response)
            if(response.success) {
                window.location.reload();
            } else {
                swal ( "Failure" ,  "Email or password not right" ,  "error" );
            }
        });
    });

    $(document).on('click', '#show_password', function(){
        var $input = $('#password');
        $input.attr('type') == 'password' ? $input.attr('type', 'text') : $input.attr('type', 'password');
    });

    $(document).on('click', '.approve_product', function(e){
        var prod_id = $(this).attr('data-id');
        console.log(equilli.ajax_url);
        $.ajax({
            method: 'GET',
            url: `${API_URL}products/flick-status/${prod_id}`,
            success: function(data){
                if(data.status.is_approved == 1){
                    swal ( "Success" ,  "Status updated" ,  "success" );
                    setTimeout(function(){
                        location.reload()
                    },1500)
                }else{
                    swal ( "Failure" ,  "Can not update status" ,  "error" );
                }
            },
            error: function(err){
                console.log(err);
            }
        });
    });

    $(document).on('click', '#logout', function() {

        var data = {
            'action': 'logout',
            'payload': {
                'type': 'chupakabra'
            }
        };

        jQuery.post(equilli.ajax_url, data, function(response) {

            response = JSON.parse(response);
            if(response.success) {
                window.location.reload();
            } else {

            }
        });
    });

    $(document).on('click', '.equilli-row-toggle', function() {

        var productId = $(this).attr('product-id');
        var editContainer = $('.equilli-product-row-edit[product-id="'+productId+'"]');

        if($(editContainer).is(":visible")) {
            $(editContainer).hide();
        } else {
            $(editContainer).show();
        }

    });



    $(document).on('click', '.equilli-select-button', function() {

        var data = {
            'action': 'save_button_type',
            'payload': {
                button: $(this).attr('button-type')
            }
        };

        jQuery.post(equilli.ajax_url, data, function(response) {
            console.log(response);
            if(response == 0){
                cal_swall('<h1>Button style updated</h1>', 'Success');
            }else{
                cal_swall('<h1>Button not updated</h1>', 'Error');
            }
        });

    });

    $(document).on('submit', '#sync-form', function(e) {
        e.preventDefault();
        $('#backdrop').css('display', 'block');
        var array = $(this).serializeArray();

        var object = {};
        for(var i in array) {

            var regex = /([a-zA-Z_\-]+)\[([0-9]*)\]/ig;
            var result = regex.exec(array[i].name);
            
            var productId = result[2];
            if(!object[productId]) {
                object[productId] = {};
            }
            
            object[productId][result[1]] = array[i].value;
        
        }

        let flag = true;
        Object.keys(object).forEach(function(key){
            if( typeof object[key]['is-active'] != 'undefined' &&
                object[key]['category'] == '0'
            ){
                $('#backdrop').css('display', 'none');
                cal_swall(`<h3 class="text-center">Please choose product category</h3>`, 'Warning');
                flag = false;
            }
        });

        if(flag){

            var data = {
                'action': 'sync',
                'payload': object
            };

            jQuery.post(equilli.ajax_url, data, function(response) {

                // var response = JSON.parse(response);
                // alert(response.synced.length + " items were synced");
                var res = JSON.parse(response.slice(0,-1));
                console.log(res)
                var final_html = '';
                var synced_html = '';
                var unsynced_html = '';
                var thead = "<thead><tr><th>ID</th><th>Name</th><th>Min Price</th></tr></thead>"
                
                res.synced.forEach(function(prod, i){
                    var prod = JSON.parse(prod);            
                    synced_html += '<tr>';
                    synced_html += `<td>${prod.id}</td>`;
                    synced_html += `<td>${prod.name}</td>`;
                    synced_html += `<td>${prod.price}</td>`;
                    synced_html += '</tr>';
                });
                
                res.unsynced.forEach(function(prod, i){
                    var prod = JSON.parse(prod);            
                    unsynced_html += '<tr>';
                    unsynced_html += `<td>${prod.id}</td>`;
                    unsynced_html += `<td>${prod.name}</td>`;
                    unsynced_html += `<td>${prod.price}</td>`;
                    unsynced_html += '</tr>';
                });
                
                if(res.synced.length > 0){
                    final_html += `<h3 class="text-center">Synced</h3><table class="table">${thead}<tbody>${synced_html}<tbody></table>`;
                }
                if(res.unsynced.length > 0){
                    final_html += `<h3 class="text-center">Unsynced</h3><table class="table">${thead}<tbody>${unsynced_html}<tbody></table>`;
                }
                $('#backdrop').css('display', 'none');
                cal_swall(final_html, 'Success');
                location.reload();
            });
        }

    });

})( jQuery );
