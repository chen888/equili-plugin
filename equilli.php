<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              Equilli
 * @since             1.0.0
 * @package           Equilli
 *
 * @wordpress-plugin
 * Plugin Name:       Equilli
 * Plugin URI:        Equilli
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Equilli
 * Author URI:        Equilli
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       equilli
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'PLUGIN_NAME_VERSION', '1.0.0' );
define( 'API_URL', 'https://e-quili.com/api/' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-equilli-activator.php
 */
function activate_equilli() {
    global $wpdb;

    require_once plugin_dir_path( __FILE__ ) . 'includes/class-equilli-activator.php';
	Equilli_Activator::activate();

    $charset_collate = $wpdb->get_charset_collate();

    $table_name1 = $wpdb->prefix . "equilli_products";
    $table_name2 = $wpdb->prefix . "equilli_settings";

    $sql1 = "CREATE TABLE $table_name1 (
    id int(11) NOT NULL AUTO_INCREMENT,
    local_id int(11) NOT NULL,
    equilli_id int(11) NOT NULL,
    data longtext COLLATE $wpdb->collate,
    PRIMARY KEY  (id)
    ) $charset_collate;";

    $sql2 = "CREATE TABLE $table_name2 (
    id int(11) NOT NULL AUTO_INCREMENT,
    option_name VARCHAR(191) COLLATE $wpdb->collate NOT NULL,
    option_value VARCHAR(1000) COLLATE $wpdb->collate NOT NULL,
    PRIMARY KEY  (id),
    UNIQUE KEY `option_name` (`option_name`)
    ) $charset_collate;";

    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
    dbDelta($sql1);
    dbDelta($sql2);

    $wpdb->query("INSERT IGNORE INTO {$table_name2} (`option_name`, `option_value`) VALUES ('button_type', '1')");
    $wpdb->query("INSERT IGNORE INTO {$table_name2} (`option_name`, `option_value`) VALUES ('delay_type', '1')");
    $wpdb->query("INSERT IGNORE INTO {$table_name2} (`option_name`, `option_value`) VALUES ('login_token', '')");
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-equilli-deactivator.php
 */
function deactivate_equilli() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-equilli-deactivator.php';
	Equilli_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_equilli' );
register_deactivation_hook( __FILE__, 'deactivate_equilli' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-equilli.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_equilli() {

    session_start();
	$plugin = new Equilli();
	$plugin->run();

}
run_equilli();
