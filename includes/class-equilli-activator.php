<?php

/**
 * Fired during plugin activation
 *
 * @link       Equilli
 * @since      1.0.0
 *
 * @package    Equilli
 * @subpackage Equilli/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Equilli
 * @subpackage Equilli/includes
 * @author     Equilli <Equilli>
 */
class Equilli_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
