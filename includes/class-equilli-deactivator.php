<?php

/**
 * Fired during plugin deactivation
 *
 * @link       Equilli
 * @since      1.0.0
 *
 * @package    Equilli
 * @subpackage Equilli/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Equilli
 * @subpackage Equilli/includes
 * @author     Equilli <Equilli>
 */
class Equilli_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
