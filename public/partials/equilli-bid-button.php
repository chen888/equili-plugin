<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>


<?php if($status == "Approved"): ?>
    <a href="https://equilli.devsense.co.il:8000/make-a-bid/product-details?iframe=wordpress&id=<?=$current_product?>&currency=<?=$currency?>" data-fancybox data-options='{"iframe" : {"css" : {"width" : "80%", "height" : "80%"}}}' data-type="iframe" id="equilli-open-popup-button" class="equilli-invisible <?=$button_class ?>">&nbsp;</button>
<?php endif; ?>

<script>
    var obj = {
        button:     '<?php echo $button_class; ?>',
        product_id: '<?php echo $current_product; ?>',
        status:     '<?php echo $status; ?>',
        currency: '<?php echo $currency; ?>'
    };

    console.log(obj);

    window.addEventListener('message', function(e){
        console.log(e.data);
        
        switch(e.data.message){
            case 'success':
                window.location = "/wp-admin/admin.php?page=equilli";
                break;
            case 'successful-bid':
                (function( $ ) {
                    $('#equilli-open-popup-button').addClass('equilli-invisible');
                 })( jQuery );
                break;
            case 'redirect-bid':
                window.location =  'https://equilli.devsense.co.il:8000/bids/'+e.data.bid_id
                break;
        }
    },false);

</script>

<?php if($delay > 1): ?>
    <?php
    if($delay == 2) {
        $jsDelay = 1000;
    } elseif($delay == 3) {
        $jsDelay = 5000;
    }
    ?>
    <script type="text/javascript">
        (function( $ ) {
            setTimeout(function() {
                $('#equilli-open-popup-button').removeClass('equilli-invisible');
            }, <?=$jsDelay?>);
        })( jQuery );
    </script>
<?php else: ?>
    <script type="text/javascript">
        (function( $ ) {
            $('#equilli-open-popup-button').removeClass('equilli-invisible');
        })( jQuery );
    </script>
<?php endif; ?>