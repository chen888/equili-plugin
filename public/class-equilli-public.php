<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       Equilli
 * @since      1.0.0
 *
 * @package    Equilli
 * @subpackage Equilli/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Equilli
 * @subpackage Equilli/public
 * @author     Equilli <Equilli>
 */
class Equilli_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/equilli-public.css', array(), $this->version, 'all' );
		wp_enqueue_style( $this->plugin_name, 'https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/equilli-public.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( $this->plugin_name, 'https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js', array( 'jquery' ), $this->version, false );

	}

	public function show_equilli_button($data) {
		global $wpdb, $product;

        $result = $wpdb->get_results($wpdb->prepare("SELECT * FROM {$wpdb->prefix}equilli_products WHERE local_id=%d", [
            $product->get_id()
		]));

        if(count($result) === 0) {
            return;
		}

		$currency = get_woocommerce_currency();
		
		$current_product = $result[0]->equilli_id;
		
		// $ch = curl_init(API_URL . "products/check-status/{$current_product}");
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://e-quili.com/api/products/check-status/{$current_product}");
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		// curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		// 		'Content-Type: application/json'
		// ));
		$result = curl_exec($ch);
		$result = json_decode($result);
		$status = $result->status;
		curl_close($ch);



        $results = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}equilli_settings WHERE option_name='delay_type' OR option_name='button_type'" );
	
		foreach($results as $result) {

            if($result->option_name == 'delay_type') {
                $delay = $result->option_value;
            } elseif ($result->option_name == 'button_type') {
                $button_class = 'equilli-make-bid-button-' . $result->option_value;
            }
		}

	    require_once 'partials/equilli-bid-button.php';
    }

}
